$(".icheck").iCheck({
    checkboxClass:  "icheckbox_square-blue",
    radioClass:     "iradio_square-blue",
    increaseArea:   "20%"
});

$('.cvdo-submit-form').on('click', function(e) {
    e.preventDefault();
    
    var formErrors = 0;
    
    $(this).parents('form').find('.cvdo-required').each(function() {
        if($(this).hasClass('cvdo-checkbox')
        || $(this).hasClass('cvdo-radio')) {
            var inputVal = $(this).find('input:checked').length;
        } else {
            var inputVal = $(this).find('input, select, textarea').val();
        }

        if(!inputVal) {
            $(this).addClass('has-danger').
            find('.form-control-feedback').
            html(': Field required! Please input information before continuing.');
            
            formErrors++;
        } else {
            $(this).removeClass('has-danger').
            find('.form-control-feedback').
            html('');
        }
    });
    
    if(0 === formErrors) {
        grecaptcha.execute();
    } else {
        grecaptcha.reset();
    }
});

function onSubmit(token) {
    //var uni
    /*
    var formId = $("#form_id").val();
    
    
    if(0 === formErrors) {
        $("#webForm" + formId).submit();
    } else {
        grecaptcha.reset();
    }*/
}