var breakPoints = [0, 510, 690, 930, 1110];

createBreakpoints($('.cvdo-poi'), breakPoints);

var googleDirections = Number($('.cvdo-poi').attr('data-google-directions'));

$(window).on('resize', function() {
    createBreakpoints($('.cvdo-poi'), breakPoints);
});

$(".siteTooltip").tooltip();

$('.slickslide').slick({
    speed: 500,
    fade: false,
    slide: 'li',
    centerMode: false,
    slidesToShow: 1,
    variableWidth: false,
    adaptiveHeight: true,
    asNavFor: '.slick-thumbs',
    arrows: false,
    centerPadding: "0px"
});

$('.slick-thumbs').slick({
    slide: 'li',
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slickslide',
    focusOnSelect: true,
    arrows: false,
    variableWidth: true,
    centerMode: false,
    //centerPadding: "0px"
});

var mapBox      = "";
var locationLat = $("meta[itemprop='latitude']").attr("content");
var locationLng = $("meta[itemprop='longitude']").attr("content");

if(typeof mapboxgl !== "undefined") {


    var geojson     = {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [locationLng, locationLat]
                    }
                }]
            };

    mapboxgl.accessToken = "pk.eyJ1IjoiamVubnRhdGUiLCJhIjoiY2luYXgwNXJrMGkxYXY4a3ZhcHczc2tibCJ9.ewXifYMLzQmQQ_4x1DTbqA";

    mapBox = new mapboxgl.Map(
                {
                    container:  "map",
                    style:      mapStyle,
                    //style:      "mapbox://styles/mapbox/streets-v9",
                    center:     [locationLng, locationLat],
                    zoom:       16
                });

    if('undefined' === typeof googleDirections
    || !googleDirections) {
        mapBox.on("load", function() {
            var directions = new MapboxDirections({
                accessToken: mapboxgl.accessToken
            });

            mapBox.addControl(new mapboxgl.NavigationControl());
            mapBox.addControl(directions, 'top-left');

            // Add a single point to the map
            mapBox.addSource("point",
                {
                    "type": "geojson",
                    "data": geojson
                });

            mapBox.addLayer({
                "id": "point",
                "type": "circle",
                "source": "point",
                "paint": {
                    "circle-radius": 10,
                    "circle-color": "#3887be"
                }
            });

            directions.setOrigin([locationLng, locationLat]);
        });
    }
    else { 
        mapBox.on("load", function() {
            mapBox.addControl(new mapboxgl.NavigationControl());

            // Add a single point to the map
            mapBox.addSource("point",
                {
                    "type": "geojson",
                    "data": geojson
                });

            mapBox.addLayer({
                "id": "point",
                "type": "circle",
                "source": "point",
                "paint": {
                    "circle-radius": 10,
                    "circle-color": "#3887be"
                }
            });

        });
    }
}